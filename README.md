yadg-scrapers
=============

This repository used to contain different scrapers used to pull information for music releases from different sites.

This repository is obsolete now. Please take a look at the [YADG master repository](https://gitlab.com/Slack06/yadg/tree/master/descgen/scraper).
